# GoHugo Book Theme

Copie van [Hugo Book Theme](https://github.com/alex-shpak/hugo-book) versie van `18 februari 2022`.
  

Wordt alleen gebruikt als buffer om de gevolgen van mogelijke `breaking changes` te voorkomen. Zal regelmatig worden geupdate met recentie versie.
